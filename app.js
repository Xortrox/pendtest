let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let path = require('path');
let knex = require('knex') ({
    client: "mysql",
    connection: {
        multipleStatements: true,
        host : 'mysql',
        user : 'mysql',
        password : 'mariblez',
        database : 'pendoria',
        pool: { min: 6, max: 60}
    }
});


app.set('view engine', 'pug');
app.set('views', './views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/admin', function (req, res) {
    res.render('admin');
});

app.post('/admin', function (req, res) {
    knex.transaction (async function (t) {
        try {
            await knex('polls').transacting(t).insert({question: req.body.question, description: req.body.description});
            let questionData = await knex('polls').select('id').orderBy('id', 'desc').limit(1);
            let questionId = 0;
            if (questionData[0] === undefined) {
                questionId = 1
            } else {
                questionId = questionData[0].id +1;
            }
            for (var i = 0; i < req.body.answers.length; i++) {
                await knex('poll_answers').transacting(t).insert({answer: req.body.answers[i], poll_id: questionId});
                await knex('poll_votes').transacting(t).insert({vote: 0, poll_id: questionId});
            }
            return res.redirect('back');
        } catch (e) {
            console.error('Error creating poll', e);
            return res.status(402).send({
                message: e
            });
        }
    })
});


app.get('/player', async function (req, res) {
    try {
        let questionData = await knex.raw('SELECT * FROM polls ORDER BY id DESC LIMIT 4');
        let answerData = await knex.raw('SELECT polls.id, GROUP_CONCAT(poll_answers.answer) as answers FROM polls INNER JOIN poll_answers on poll_answers.poll_id = polls.id GROUP BY polls.id ORDER by polls.id DESC LIMIT 4');
        let voteData = await knex.raw('SELECT polls.id, GROUP_CONCAT(poll_votes.vote) as votes, GROUP_CONCAT(poll_votes.id) as id FROM polls INNER JOIN poll_votes on poll_votes.poll_id = polls.id GROUP BY polls.id ORDER by polls.id DESC LIMIT 4');
        let questions = questionData[0];
        for (let i = 0; i < questionData[0].length; i++) {
            questions[i].voteId = voteData[0][i].id.split(',');
            questions[i].votes = voteData[0][i].votes.split(',');
            questions[i].answers = answerData[0][i].answers.split(',');
        }
        for (let i = 0; i < questions.length; i++) {
            questions[i].max = 0;
            for (let vote of questions[i].votes) {
                questions[i].max += parseInt(vote, 10);
            }
        }
        res.render('player', {
            questions: questions
        });
    } catch (e) {
        console.error('Error getting polls', e);
        return res.status(401).send({
            message: 'Error getting polls.'
        });
    }
});


app.post('/player', async function (req, res) {
    try {
        let voteId = Object.values(req.body);
        await knex('poll_votes').increment('poll_votes.vote', 1).where('poll_votes.id', voteId);
        return res.redirect('back');
    } catch (e) {
        console.error('Error posting poll:', e);
        return res.status(400).send({
            message: 'Error posting poll.'
        });
    }
});

app.listen(8080, () => {
    console.log('Server running on http://localhost:8080')
});

console.log("we")