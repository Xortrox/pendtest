exports.up = function(knex, Promise) {
    return new Promise(async (resolve, reject) => {
        try {
            await knex.schema.createTableIfNotExists('polls', function (table) {
                table.increments('id').primary().unsigned();
                table.string('question').notNullable();
                table.string('description').notNullable();
            });
            console.log('Polls table created');
            await knex.schema.createTableIfNotExists('poll_answers', function (table) {
                table.increments('id').primary().unsigned();
                table.integer('poll_id').unsigned().index().references('id').inTable('polls').notNull();
                table.string('answer');
            });
            console.log('Poll answers table created');
            await knex.schema.createTableIfNotExists('poll_votes', function (table) {
                table.increments('id').primary().unsigned();
                table.integer('poll_id').unsigned().index().references('id').inTable('polls').notNull();
                table.string('vote');
            })
        } catch (e) {
            reject(e)
        }
    })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('poll_answers')
        .dropTable('poll_votes')
        .dropTable('polls')
};